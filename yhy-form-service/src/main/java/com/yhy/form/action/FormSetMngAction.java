package com.yhy.form.action;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.yhy.admin.api.IAdminService;
import com.yhy.admin.dto.DataDictApiDTO;
import com.yhy.common.action.BaseComplexMngAction;
import com.yhy.common.constants.BusSelfState;
import com.yhy.common.constants.FunProcess;
import com.yhy.common.dto.AppReturnMsg;
import com.yhy.common.dto.ReturnCode;
import com.yhy.common.intercept.PreCheckPermission;
import com.yhy.common.utils.JsonUtils;
import com.yhy.common.utils.SpringContextHolder;
import com.yhy.common.utils.YhyUtils;
import com.yhy.common.vo.SysUser;
import com.yhy.form.dto.FormFieldPrivDTO;
import com.yhy.form.dto.FormSetDTO;
import com.yhy.form.service.FormSetContentService;
import com.yhy.form.service.FormSetFieldPrivService;
import com.yhy.form.service.FormSetFieldService;
import com.yhy.form.service.FormSetMainService;
import com.yhy.form.service.mng.FormSetMngService;
import com.yhy.form.vo.FormSetContentVO;
import com.yhy.form.vo.FormSetFieldPrivVO;
import com.yhy.form.vo.FormSetFieldVO;
import com.yhy.form.vo.FormSetMainVO;
import com.yhy.form.vo.mng.FormSetMngVO;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-1-10 下午2:50 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *  
 */
@RestController
@RequestMapping(value="/form-service/api/formSetMng", produces="application/json;charset=UTF-8")
public class FormSetMngAction extends BaseComplexMngAction<FormSetDTO> {

    @Autowired
    private FormSetMainService formSetMainService;

    @Autowired
    private FormSetContentService formSetContentService;
    @Autowired
    private FormSetFieldService formSetFieldService;

    @Autowired
    private FormSetFieldPrivService formSetFieldPrivService;

    @Autowired
    @Qualifier(IAdminService.SERVICE_BEAN)
    private IAdminService adminService;


    @Override
    protected FormSetMngService getBaseService() {
        return SpringContextHolder.getBean(FormSetMngService.class);
    }

    @Override
    protected void afterLoadDataInfo(FormSetDTO baseDTO, AppReturnMsg returnMsg ) {

    }

    @RequestMapping(value="/getFormContent",method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value="根据ID获取表单内容", notes="根据ID获取表单内容")
    public AppReturnMsg getFormTreeData(@RequestParam("mainId")String mainId, HttpServletRequest request, HttpServletResponse response) {
        FormSetContentVO formSetContentVO =  formSetContentService.findByMainId(mainId);
        return new AppReturnMsg(ReturnCode.SUCCESS_CODE.getCode(),"", formSetContentVO,null);
    }

    @RequestMapping(value="/getExtendFormInfo",method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value="根据业务类型获取扩展表单模板ID", notes="根据业务类型获取扩展表单模板ID")
    public AppReturnMsg getExtendFormInfo(@RequestParam("moduleBusClass")String moduleBusClass, HttpServletRequest request, HttpServletResponse response) {
        FormSetMngVO formInfo =  getBaseService().getFormExtendInfoByModuleBusClass(moduleBusClass,YhyUtils.getSysUser().getCpyCode());
        return new AppReturnMsg(ReturnCode.SUCCESS_CODE.getCode(),"", formInfo == null ? new FormSetMngVO() : formInfo,null);
    }

    @RequestMapping(value="/getFormApplyTreeData",method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value="获取有权限业务申请表单菜单", notes="获取有权限业务申请表单菜单")
    public AppReturnMsg getFormApplyTreeData(HttpServletRequest request, HttpServletResponse response) {
        SysUser sysUser = YhyUtils.getSysUser();
        //1. 根据用户 获取有权限的表单 code
        Set<String> formCodes = adminService.getRoleFromByUserAccount(sysUser.getUserAccount(), sysUser.getCpyCode());
        //2. 根据表单code进行获取
        List<FormSetMngVO> rtnTree = Lists.newArrayList();
        if(!formCodes.isEmpty()) {
            List<FormSetMngVO> rtnList = getBaseService().getFormSetByCode(formCodes);
            rtnTree = getBaseService().buildFormApplyTree(rtnList);
        }

        DataDictApiDTO dataDictApiDTO = adminService.getDataDictByTypeAndCode("form_type","OTHER",sysUser.getLanCode());
        // 增加其它应用表单
        FormSetMngVO otherMenu = new FormSetMngVO();
        otherMenu.setFormName(dataDictApiDTO.getDictName());
        otherMenu.setFormType(dataDictApiDTO.getDictCode());
        otherMenu.setId(YhyUtils.generateUUID());
        rtnTree.add(otherMenu);

        return new AppReturnMsg(ReturnCode.SUCCESS_CODE.getCode(),"", rtnTree,null);
    }


    @PreCheckPermission(roles = {"SYS_ADMIN","COMMON_ADMIN","FORM_SET_ALL","FORM_SET_CHANGE"})
    @RequestMapping(value="/toChange",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="变更操作", notes="变更操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "提交对象ID", required = true, dataTypeClass = FormSetDTO.class)
    })
    public AppReturnMsg toChange(HttpServletRequest request, HttpServletResponse response, @RequestBody FormSetDTO baseDTO) {
        return super.toChangeCommon(request, response, baseDTO);
    }

    @PreCheckPermission(roles = {"SYS_ADMIN","COMMON_ADMIN","FORM_SET_ALL","FORM_SET_CREATE"})
    @RequestMapping(value="/toCopy",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="复制操作", notes="复制操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "提交对象ID", required = true, dataTypeClass = FormSetDTO.class)
    })
    public AppReturnMsg toCopy(HttpServletRequest request, HttpServletResponse response, @RequestBody FormSetDTO baseDTO) {
        return super.toCopyCommon(request, response, baseDTO);
    }

    @PreCheckPermission(roles = {"SYS_ADMIN","COMMON_ADMIN","FORM_SET_ALL","FORM_SET_CREATE"})
    @RequestMapping(value="/toAdd",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="新增操作", notes="新增操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "提交对象ID", required = true, dataTypeClass = FormSetDTO.class)
    })
    public AppReturnMsg toAdd(HttpServletRequest request, HttpServletResponse response, @RequestBody FormSetDTO baseDTO) {
        return super.toAddCommon(request, response, baseDTO);
    }

    @PreCheckPermission(roles = {"SYS_ADMIN","COMMON_ADMIN","FORM_SET_ALL","FORM_SET_CHANGE","FORM_SET_CREATE"})
    @RequestMapping(value="/doSave",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="保存", notes="保存操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "数据传输对象", required = true, dataTypeClass = FormSetDTO.class)
    })
    public AppReturnMsg doSave(HttpServletRequest request, HttpServletResponse response, @RequestBody FormSetDTO baseDTO) {
        return super.doSaveCommon(request, response, baseDTO);
    }

    @PreCheckPermission(roles = {"SYS_ADMIN","COMMON_ADMIN","FORM_SET_ALL","FORM_SET_SELECT"})
    @RequestMapping(value="/loadData",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="查询数据", notes="查询操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "数据传输对象", required = true, dataTypeClass = FormSetDTO.class)
    })
    public AppReturnMsg loadData(HttpServletRequest request, HttpServletResponse response, @RequestBody FormSetDTO baseDTO) {
        return super.loadDataCommon(request, response, baseDTO);
    }

    @PreCheckPermission(roles = {"SYS_ADMIN","COMMON_ADMIN","FORM_SET_ALL","FORM_SET_DELETE"})
    @RequestMapping(value="/doDelete",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="删除", notes="删除操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "删除", required = true, dataTypeClass = FormSetDTO.class)
    })
    public AppReturnMsg doDelete(HttpServletRequest request, HttpServletResponse response, @RequestBody FormSetDTO baseDTO) {
        return super.doDeleteCommon(request, response, baseDTO);
    }

    @PreCheckPermission(roles = {"SYS_ADMIN","COMMON_ADMIN","FORM_SET_ALL","FORM_SET_SUBMIT"})
    @RequestMapping(value="/doSubmit",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="提交", notes="提交操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "提交对象ID", required = true, dataTypeClass = FormSetDTO.class)
    })
    public AppReturnMsg doSubmit(HttpServletRequest request, HttpServletResponse response, @RequestBody FormSetDTO baseDTO) {
        return super.doSubmitCommon(request, response, baseDTO);
    }

    @PreCheckPermission(roles = {"SYS_ADMIN","COMMON_ADMIN","FORM_SET_ALL","FORM_SET_DESTORY"})
    @RequestMapping(value="/doDestory",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="注销操作", notes="注销操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "提交对象ID", required = true, dataTypeClass = FormSetDTO.class)
    })
    public AppReturnMsg doDestory(HttpServletRequest request, HttpServletResponse response, @RequestBody FormSetDTO baseDTO) {
        return super.doDestoryCommon(request, response, baseDTO);
    }

    @RequestMapping(value="/getFeildDetailInfo",method = {RequestMethod.GET})
    @ResponseBody
    @ApiOperation(value="根据主ID获取控件列表", notes="根据主ID获取控件列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "mainId", value = "主表ID", required = true, dataTypeClass = String.class)
    })
    public AppReturnMsg getFeildDetailInfo(HttpServletRequest request, HttpServletResponse response,
                                            @RequestParam("mainId") String mainId) {
        List<FormSetFieldVO> formSetFieldVOList = formSetFieldService.findByMainId(mainId);
        return new AppReturnMsg(ReturnCode.SUCCESS_CODE.getCode(),null,formSetFieldVOList);
    }

    @PreCheckPermission(roles = {"SYS_ADMIN","COMMON_ADMIN","FORM_SET_ALL","FORM_SET_CHANGE","FORM_SET_CREATE"})
    @RequestMapping(value="/doSaveFieldPriv",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="保存控件权限设置", notes="保存控件权限设置")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "权限配置信息", value = "权限配置信息", required = true, dataTypeClass = FormSetDTO.class)
    })
    public AppReturnMsg doSaveFieldPriv(HttpServletRequest request, HttpServletResponse response,
                                        @RequestBody FormFieldPrivDTO baseDTO) {
        getBaseService().saveFieldPriv(baseDTO.getFieldVO(), baseDTO.getVisibleFieldPrivVO(), baseDTO.getEditFieldPrivVO());
        return new AppReturnMsg(ReturnCode.SUCCESS_CODE.getCode(),"保存成功","");
    }

    @PreCheckPermission(roles = {"SYS_ADMIN","COMMON_ADMIN","FORM_SET_ALL","FORM_SET_CHANGE","FORM_SET_CREATE"})
    @RequestMapping(value="/doSaveFormDesign",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="保存表单设计", notes="保存表单设计")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "表单配置信息", value = "表单配置信息", required = true, dataTypeClass = FormSetDTO.class)
    })
    public AppReturnMsg doSaveFormDesign(HttpServletRequest request, HttpServletResponse response,
                                        @RequestBody FormSetDTO baseDTO) {
        getBaseService().saveFormDesign(baseDTO);
        return new AppReturnMsg(ReturnCode.SUCCESS_CODE.getCode(),"保存成功",baseDTO);
    }

    @RequestMapping(value="/getFieldPrivInfo",method = {RequestMethod.GET})
    @ResponseBody
    @ApiOperation(value="根据主ID及控件ID获取对应权限明细", notes="根据主ID及控件ID获取对应权限明细")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "fieldId", value = "控件表ID", required = true, dataTypeClass = String.class)
    })
    public AppReturnMsg getFieldPrivInfo(HttpServletRequest request, HttpServletResponse response,
                                            @RequestParam("fieldId") String fieldId) {
        List<FormSetFieldPrivVO> fieldPrivVOS = formSetFieldPrivService.findByFieldId(fieldId);
        FormFieldPrivDTO configInfo = formSetFieldPrivService.convertDetail2Info(fieldId, fieldPrivVOS);
        return new AppReturnMsg(ReturnCode.SUCCESS_CODE.getCode(),null,configInfo);
    }

    @RequestMapping(value="/findFormByCpyCode",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="根据公司获取所有表单信息", notes="根据公司获取所有用户信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "传入参数对象", required = true, dataTypeClass = FormSetDTO.class),

    })
    public AppReturnMsg findFormByCpyCode(HttpServletRequest request, HttpServletResponse response,
                                          @RequestBody FormSetDTO baseDTO) {
        baseDTO.getParamBean().setSelfState(BusSelfState.VALID.getVal());
        baseDTO.setRtnList(getBaseService().queryPageData(baseDTO));
        return new AppReturnMsg(ReturnCode.SUCCESS_CODE.getCode(),"",baseDTO);
    }

}