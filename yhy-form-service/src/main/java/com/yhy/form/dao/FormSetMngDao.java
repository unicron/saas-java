package com.yhy.form.dao;

import com.yhy.common.dao.BaseComplexMngDao;
import com.yhy.form.dto.FormSetDTO;
import com.yhy.form.vo.mng.FormSetMngVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-1-10 上午11:05 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */

@Mapper
@Component(value = "formSetMngDao")
public interface FormSetMngDao extends BaseComplexMngDao<FormSetDTO, FormSetMngVO> {

    List<FormSetMngVO> getFormSetByCode(@Param("codes") Set<String> codes);

    Boolean hasFormExtendInfoByModuleBusClass(@Param("moduleBusClass") String moduleBusClass,@Param("sysOwnerCpy") String sysOwnerCpy,@Param("sysGenCode") String sysGenCode);

    FormSetMngVO getFormExtendInfoByModuleBusClass(@Param("moduleBusClass") String moduleBusClass,@Param("sysOwnerCpy") String sysOwnerCpy);

}
