package com.yhy.admin.dto;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-4-9 上午10:07 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */

import com.yhy.common.dto.BaseEntity;

public class OrgSetApiDTO extends BaseEntity {


    private String orgName;
    private String orgType;
    private String parentId;
    private String orgCode;
    private String cpyCode;
    private String cpyName;
    private String thirdCode;
    private String orgStatus;
    private String mgrAccount;

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getOrgType() {
        return orgType;
    }

    public void setOrgType(String orgType) {
        this.orgType = orgType;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getOrgCode() {
        return orgCode;
    }

    public void setOrgCode(String orgCode) {
        this.orgCode = orgCode;
    }

    public String getCpyCode() {
        return cpyCode;
    }

    public void setCpyCode(String cpyCode) {
        this.cpyCode = cpyCode;
    }

    public String getCpyName() {
        return cpyName;
    }

    public void setCpyName(String cpyName) {
        this.cpyName = cpyName;
    }

    public String getThirdCode() {
        return thirdCode;
    }

    public void setThirdCode(String thirdCode) {
        this.thirdCode = thirdCode;
    }

    public String getOrgStatus() {
        return orgStatus;
    }

    public void setOrgStatus(String orgStatus) {
        this.orgStatus = orgStatus;
    }

    public String getMgrAccount() {
        return mgrAccount;
    }

    public void setMgrAccount(String mgrAccount) {
        this.mgrAccount = mgrAccount;
    }
}
