package com.yhy.admin.dto;

import com.yhy.admin.vo.mng.OrgSetMngVO;
import com.yhy.admin.vo.OrgSetVO;
import com.yhy.admin.vo.UserOrgSetVO;
import com.yhy.common.dto.BaseMngDTO;

import java.util.List;

public class OrgSetDTO extends BaseMngDTO<OrgSetMngVO> {


    private List<UserOrgSetVO> userOrgSetVOList;

    @Override
    public OrgSetVO getBusMain() {
        return new OrgSetVO();
    }

    public List<UserOrgSetVO> getUserOrgSetVOList() {
        return userOrgSetVOList;
    }

    public void setUserOrgSetVOList(List<UserOrgSetVO> userOrgSetVOList) {
        this.userOrgSetVOList = userOrgSetVOList;
    }
}
