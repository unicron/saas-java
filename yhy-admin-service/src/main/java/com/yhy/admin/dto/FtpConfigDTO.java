package com.yhy.admin.dto;

import com.yhy.admin.vo.FtpConfigSet;
import com.yhy.admin.vo.mng.FtpConfigMngVO;
import com.yhy.common.dto.BaseMngDTO;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-3-25 下午1:56 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */
public class FtpConfigDTO extends BaseMngDTO<FtpConfigMngVO> {

    @Override
    public FtpConfigSet getBusMain() {
        return new FtpConfigSet();
    }

}
