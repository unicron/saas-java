package com.yhy.admin.vo;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.yhy.common.dto.BaseEntity;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-4-24 上午9:08 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */
public class SysCustomFieldVO extends BaseEntity {
    /**
     * 表单ID db_column: GRID_ID 
     */	
	private String gridId;
    /**
     * 列字段名 db_column: FIELD_PROP 
     */	
	private String fieldProp;
    /**
     * 列名称 db_column: FIELD_LABEL 
     */	
	private String fieldLabel;
    /**
     * 列宽度 db_column: FIELD_WIDTH 
     */	
	private Integer fieldWidth;
	private Integer fieldVisible;
    /**
     * 字段排序 db_column: FIELD_SORT 
     */	
	private Integer fieldSort;
    /**
     * 模块 db_column: BUS_MODULE 
     */	
	private String busModule;
    /**
     * 业务种类 db_column: BUS_CLASS 
     */	
	private String busClass;

	public SysCustomFieldVO(){
	}

	public SysCustomFieldVO( String id ){
		this.id = id;
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}
	public String getGridId() {
		return this.gridId;
	}
	
	public void setGridId(String gridId) {
		this.gridId = gridId;
	}
	public String getFieldProp() {
		return this.fieldProp;
	}
	
	public void setFieldProp(String fieldProp) {
		this.fieldProp = fieldProp;
	}
	public String getFieldLabel() {
		return this.fieldLabel;
	}
	
	public void setFieldLabel(String fieldLabel) {
		this.fieldLabel = fieldLabel;
	}
	public Integer getFieldWidth() {
		return this.fieldWidth;
	}
	
	public void setFieldWidth(Integer fieldWidth) {
		this.fieldWidth = fieldWidth;
	}
	public Integer getFieldSort() {
		return this.fieldSort;
	}
	
	public void setFieldSort(Integer fieldSort) {
		this.fieldSort = fieldSort;
	}
	public String getBusModule() {
		return this.busModule;
	}
	
	public void setBusModule(String busModule) {
		this.busModule = busModule;
	}
	public String getBusClass() {
		return this.busClass;
	}
	
	public void setBusClass(String busClass) {
		this.busClass = busClass;
	}

	public Integer getFieldVisible() {
		return fieldVisible;
	}

	public void setFieldVisible(Integer fieldVisible) {
		this.fieldVisible = fieldVisible;
	}
}

