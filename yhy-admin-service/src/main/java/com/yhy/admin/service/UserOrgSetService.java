package com.yhy.admin.service;

import com.yhy.admin.dao.UserOrgSetDao;
import com.yhy.admin.vo.UserOrgSetVO;
import com.yhy.common.service.BaseMainService;
import com.yhy.common.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.List;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 19-7-5 上午9:51 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2019<br>
 *  *
 *
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class UserOrgSetService extends BaseService<UserOrgSetVO> {

    @Autowired
    private UserOrgSetDao userOrgSetDao;

    @Override
    protected UserOrgSetDao getDao() {
        return userOrgSetDao;
    }

    @Override
    protected void preInsert(UserOrgSetVO entity) {
        super.preInsert(entity);
    }

    public List<UserOrgSetVO> getOrgSetInfoByUserAccount(String userAccount, String cpyCode) {
        UserOrgSetVO paramBean = new UserOrgSetVO();
        paramBean.setUserAccount(userAccount);
        paramBean.setCpyCode(cpyCode);
        paramBean.setEnableFlag("Y");
        return getDao().findBy(paramBean);
    }

    public UserOrgSetVO getUniqueOrgSetInfoByUserAccount(String userAccount, String cpyCode) {
        UserOrgSetVO paramBean = new UserOrgSetVO();
        paramBean.setUserAccount(userAccount);
        paramBean.setCpyCode(cpyCode);
        paramBean.setEnableFlag("Y");
        List<UserOrgSetVO> rtnList = getDao().findBy(paramBean);
        if(CollectionUtils.isEmpty(rtnList)) {
            return null;
        }
        return rtnList.get(0);
    }

}
