package com.yhy.admin.service;

import com.yhy.common.exception.BusinessException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.yhy.admin.dao.SysUserJobDao;
import com.yhy.admin.vo.SysUserJobVO;
import com.yhy.common.service.BaseService;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-3-17 下午3:36 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */

@Service
@Transactional(rollbackFor = Exception.class)
public class SysUserJobService extends BaseService<SysUserJobVO> {

	@Autowired
	private  SysUserJobDao sysUserJobDao;

	@Override
	protected SysUserJobDao getDao() {
		return sysUserJobDao;
	}

	public SysUserJobVO findJobByUserId(String cpyCode, String userId) {
		return getDao().findJobByUserId(cpyCode, userId);
	}

	public Integer deleteByUserId(String cpyCode, String userId) {
		return getDao().deleteByUserId(cpyCode, userId);
	}

	public void saveUserJob(SysUserJobVO userJobVO) {
		this.deleteByUserId(userJobVO.getCpyCode(),userJobVO.getUserId());
		if(StringUtils.isNotBlank(userJobVO.getJobId())) {
			userJobVO.setEnableFlag("Y");
			insert(userJobVO);
		}
	}

}
