package com.yhy.admin.service;

import com.yhy.admin.dao.SysRoleUserDao;
import com.yhy.admin.vo.SysRoleUserVO;
import com.yhy.admin.vo.mng.SysUserMngVO;
import com.yhy.common.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 19-8-8 下午6:24 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2019<br>
 *  *
 *
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class SysRoleUserService extends BaseService<SysRoleUserVO> {


    @Autowired
    private SysRoleUserDao baseDao;

    @Override
    protected SysRoleUserDao getDao() {
        return baseDao;
    }

    public List<SysUserMngVO> findUserByRoleId(String roleId) {
        return getDao().findUserByRoleId(roleId);
    }

}
