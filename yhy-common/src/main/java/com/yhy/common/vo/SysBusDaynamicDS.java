package com.yhy.common.vo;

import com.yhy.common.dto.BaseEntity;

public class SysBusDaynamicDS extends BaseEntity {

    /**dbKey*/
    private String dbKey;
    /**description*/
    private String description;
    /**driverClass*/
    private String driverClass;
    /**url*/
    private String url;
    /**dbUser*/
    private String dbUser;
    /**dbPassword*/
    private String dbPassword;
    /**dbType*/
    private String dbType;
    /**dbName*/
    private String dbName;

    public SysBusDaynamicDS() {
    }

    public String getDbKey() {
        return dbKey;
    }

    public void setDbKey(String dbKey) {
        this.dbKey = dbKey;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDriverClass() {
        return driverClass;
    }

    public void setDriverClass(String driverClass) {
        this.driverClass = driverClass;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDbUser() {
        return dbUser;
    }

    public void setDbUser(String dbUser) {
        this.dbUser = dbUser;
    }

    public String getDbPassword() {
        return dbPassword;
    }

    public void setDbPassword(String dbPassword) {
        this.dbPassword = dbPassword;
    }

    public String getDbType() {
        return dbType;
    }

    public void setDbType(String dbType) {
        this.dbType = dbType;
    }

    public String getDbName() {
        return dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }
}
