package com.yhy.common.dto;

import com.yhy.common.utils.ToStringBean;
import com.yhy.common.utils.YhyUtils;
import com.yhy.common.vo.BusCommonState;
import com.yhy.common.vo.OrderVO;
import com.yhy.common.vo.PageVO;
import com.yhy.common.vo.SysUser;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BaseDTO <E extends BaseEntity> extends ToStringBean implements Serializable {

    private PageVO pageBean;

    protected E paramBean;

    private List<E> rtnList;

    private Map<String, Object> otherParams = new HashMap();

    private String lanCode;

    private String sourceType;

    private String busModule;
    private String busClass;

    private String pageQueryCount;

    private SysUser sysUser;


    private List<String> idList;

    public BaseDTO() {

    }

    public PageVO getPageBean() {
        return pageBean;
    }

    public void setPageBean(PageVO pageBean) {
        this.pageBean = pageBean;
    }

    public List<String> getIdList() {
        return idList;
    }

    public void setIdList(List<String> idList) {
        this.idList = idList;
    }

    public String getBusClass() {
        return busClass;
    }

    public void setBusClass(String busClass) {
        this.busClass = busClass;
    }

    public SysUser getSysUser() {
        return sysUser;
    }

    public void setSysUser(SysUser sysUser) {
        this.sysUser = sysUser;
    }

    public String getPageQueryCount() {
        return pageQueryCount;
    }

    public void setPageQueryCount(String pageQueryCount) {
        this.pageQueryCount = pageQueryCount;
    }

    public String getBusModule() {
        return busModule;
    }

    public void setBusModule(String busModule) {
        this.busModule = busModule;
    }

    public List<E> getRtnList() {
        return rtnList;
    }

    public void setRtnList(List<E> rtnList) {
        this.rtnList = rtnList;
    }

    public E getParamBean() {
        return paramBean;
    }

    public void setParamBean(E paramBean) {
        this.paramBean = paramBean;
    }

    public Map<String, Object> getOtherParams() {
        return otherParams;
    }

    public void setOtherParams(Map<String, Object> otherParams) {
        this.otherParams = otherParams;
    }

    public void addOtherParam(String otherKey,Object value) {
        this.otherParams.put(otherKey,value);
    }

    public void removeOtherParam(String otherKey) {
        this.otherParams.remove(otherKey);
    }

    public String getSourceType() {
        if(sourceType == null) {
            sourceType = "PC";//app
        }
        return sourceType;
    }

    public void setSourceType(String sourceType) {
        this.sourceType = sourceType;
    }

    public String getLanCode() {
        if(StringUtils.isBlank(lanCode) && YhyUtils.getSysUser() != null) {
            lanCode = YhyUtils.getSysUser().getLanCode();
        }
        return lanCode;
    }

    public void setLanCode(String lanCode) {
        this.lanCode = lanCode;
    }
}
