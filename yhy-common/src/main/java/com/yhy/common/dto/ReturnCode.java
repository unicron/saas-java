package com.yhy.common.dto;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by yanghuiyuan on 2017/11/6.
 */
public enum ReturnCode {

    SUCCESS_CODE("1000","成功"),

    LOGIN_FAIL("-99","登录失败"),

    PRIV_FAIL("-100","无效权限"),

    FAIL_CODE("-1000","失败");

    private final String code;

    private final String msg;

    private static Map<String, ReturnCode> returnCodeMap;

    private ReturnCode(String code) {
        this.code = code;
        this.msg = null;
    }

    private ReturnCode(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

    /**
     * 根据code获取错误类型
     * @param code
     * @return
     */
    public static ReturnCode getInstByCode(int code) {
        synchronized (ReturnCode.class) {
            if (returnCodeMap == null) {
                returnCodeMap = new HashMap<String, ReturnCode>();
                for (ReturnCode returnCode : ReturnCode.values()) {
                    returnCodeMap.put(returnCode.getCode(), returnCode);
                }
            }
        }
        if (!returnCodeMap.containsKey(code)) {
            return null;
            //throw new BikeException(code + "对应的ReturnCode枚举值不存在。");
        }
        return returnCodeMap.get(code);
    }

}
