package com.yhy.job.dto;

import com.yhy.common.dto.BaseMngDTO;
import com.yhy.job.vo.mng.QuartzJobMngVO;
import com.yhy.job.vo.QuartzJobVO;


/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 19-8-16 下午2:45 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2019<br>
 *  *
 *
 */

public class QuartzJobDTO extends BaseMngDTO<QuartzJobMngVO> {


    @Override
    public QuartzJobVO getBusMain() {
        return new QuartzJobVO();
    }

}
